import React, { useState, useEffect, useCallback } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'

import { Header } from './components/Header'
import { TodoList } from './components/TodoList'
import { TodoForm } from './components/TodoForm'

import './style.css'

//root component
  const App = () => {

    const [todos, setTodos]= useState([])
    const [filter, setFilter] = useState('')
    const [editingId, setEditingId] = useState('')
    
    const loadTodos = useCallback(async () => {
      const response = await axios.get('http://localhost:3001/todos');
      setTodos(response.data)
    }, [])


    const loadFavorites = useCallback(async () => {
      const response = await axios.get('http://localhost:3001/todos/favorites');
      setTodos(response.data) 
    }, )

    useEffect( () => {
        loadTodos();
    }, [loadTodos]) 

    const addTodo = async (todo) => {
        const newTodo ={
        name: todo.name,
        description: todo.description,
        dueDate: todo.dueDate,
        completed: false,
        favorite: false
      }
      await axios.post('http://localhost:3001/todos', newTodo)
      loadTodos();
      
    }

    const removeTodo = async (id) => {
    await axios.delete('http://localhost:3001/todos/' + id)
    loadTodos();
    }

    const completeTodo = async (id) => {
      await axios.put('http://localhost:3001/todos/' + id,{
      completed: true})
      loadTodos();

    }

    const undoTodo = async (id) => {
      await axios.put('http://localhost:3001/todos/' + id,{completed: false})
      loadTodos();

    }

    const favoriteTodo = async (todo) => {
      const newValue = !todo.favorite;
      await axios.put(`http://localhost:3001/todos/${todo.id}`,{
      favorite: newValue})
      loadTodos();

    }
    
    const updateTodo = async (todo) => {
      await axios.put(`http://localhost:3001/todos/${todo.id}`, todo);
      setEditingId('');
      loadTodos();
    }

    const removeAllClick = async () => {
      await axios.delete('http://localhost:3001/todos/delete/all')
      loadTodos();
       }

    const filterTodos = async (filter) => {
      setFilter(filter)
      loadFavorites();
    }

    const filterAll = async (filter) => {
      setFilter(filter)
      loadTodos();
    }

    const startEditing = (id) => {
      setEditingId(id);
    }

    const getMeTodoThatIEdit = () => {
      const todo = todos.find((todo) => todo.id === editingId);
      return todo;
    }

    return (
      <div className="app">
        <div className="container">
          <Header appName="UROB ZMENU 2021" title="My TODO list" isVisible={true} subtitle="Everything is hard, before it's easy."></Header>
          <div className = "content">
            <TodoForm todo={getMeTodoThatIEdit()} onAdd={addTodo} onUpdate={updateTodo}></TodoForm>
            <TodoList todos={todos}
               onRemove={removeTodo} 
               onEdit={startEditing} 
               onComplete={completeTodo} 
               onUndo={undoTodo}
               onRemoveAll={removeAllClick}
               onFavorite={favoriteTodo}
               onFilter={filterTodos}
               onShow={filterAll}
            />
          </div>
        </div>
      </div>
    )
  }

ReactDOM.render(
      <App/>,
    document.getElementById('root')
);