import React, { useEffect, useState } from 'react'
import './TodoForm.css'

export const TodoForm = (props) => {

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [dueDate, setDueDate] = useState('');

    useEffect(() => {
      if (props.todo) {
        setName(props.todo.name);
        setDescription(props.todo.description);
        setDueDate(props.todo.dueDate);
      }
    },[props.todo])

    const onFormSubmit = (e) => {
      e.preventDefault(); //prevent full page refresh
      if(props.todo === undefined) {
        props.onAdd({name, description, dueDate})
      } else {
        props.onUpdate({id: props.todo.id, name, description, dueDate})
      }
      setName('')
      setDescription('') 
      setDueDate('')
      
    }

    return (
      <div className="todo-form">
        <h4 className = "todo-add">{props.todo ? 'Edit' : 'Add'} TODO</h4>
        <form onSubmit={onFormSubmit}>
          <input required className="form-input" type='text' value={name} onChange={(e) => setName(e.target.value)} placeholder="Add TODO"/>
          <textarea className="form-text" value={description} onChange={(e) => setDescription(e.target.value)} placeholder="...add some details"></textarea>
          <input required className="form-input" type="date" value={dueDate} onChange={(e) => setDueDate(e.target.value)}></input>
           <button className="button button-add" disabled={name.length<1 || !dueDate}>{props.todo ? 'Edit' : '+ Add'} Task</button> 
            {props.editing && <button className="button" onClick={props.onCancel} >Cancel</button>}
          
        </form>
      </div>
    )
  }
  