import React from 'react'
import './Header.css'
import organized from './organized.jpg';


export const Header = (props) => {
    if (props.isVisible === true) {
      return (
      <div className="header">
        <h1 className="title">{props.title}</h1>
        <h3 className="subtitle">{props.subtitle}</h3>
        <img className="header-img" src={organized} alt="organized"/>
      </div>)
    } else {
      return null;
    }
  }