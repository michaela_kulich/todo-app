import React from 'react'

import { Todo } from './Todo'
import './TodoList.css'

import heart from './heart.png';

export const TodoList = (props) => {
    const todos = props.todos;
    if (todos.length === 0){
      return <p className="todo-nothing">All done!</p>
    } else {
      return (
      <div className="todo-list">
        <div className ="total">
          <p>Total number of tasks: {todos.length}</p>
        </div>
        <div className="todo-header">
          <img className="button-filter" onClick={() => props.onFilter('favorites')} src={heart} alt='filter'/>
          <button className="button-filter-all" onClick={() => props.onShow()}>Show All</button>
        </div>
        {todos.map( (todo => {
          return (
          <div className="todo-row" key={todo.id}>
            <Todo todo={todo} 
                onUndo={props.onUndo}
                onComplete={props.onComplete}
                onEdit={props.onEdit}
                onRemove={props.onRemove}
                onFavorite={props.onFavorite}>  
            </Todo>
           </div>
          )})
        )}
        <div>
          <button className="button button-remove-all" onClick={() => props.onRemoveAll()}>Remove all</button>
        </div>
      </div>
      );
    }
  }